package com.funcionario.example.informacaofuncionario.service;

import java.time.LocalDate;
import java.util.Map.Entry;
import java.util.Objects;
import java.util.TreeMap;
import java.util.stream.Stream;

import org.springframework.stereotype.Service;

import com.funcionario.example.informacaofuncionario.model.Pessoa;

@Service
public class PessoaService {

	public TreeMap<Integer, Pessoa> peoplesMap;

	public PessoaService() {
		this.peoplesMap = new TreeMap<>();

		this.peoplesMap.put(7, new Pessoa(7l, "Sther", LocalDate.of(1994, 12, 15), LocalDate.of(2019, 8, 1)));
		this.peoplesMap.put(2, new Pessoa(2l, "Paulo", LocalDate.of(1990, 1, 16), LocalDate.of(2016, 6, 1)));
		this.peoplesMap.put(3, new Pessoa(3l, "Matheus", LocalDate.of(2000, 4, 6), LocalDate.of(2020, 10, 5)));
	}

	public void addUpdatePerson(Integer key, Pessoa pessoa) {
		this.peoplesMap.put(key, pessoa);
	}

	public void removePerson(Integer Key) {
		this.peoplesMap.remove(Key);
	}

	public Pessoa updatePerson(Long id, Pessoa pessoaRequest) {

		Pessoa pessoa = this.peoplesMap.get(id.intValue());

		if (!Objects.isNull(pessoaRequest.getNome())) {
			pessoa.setNome(pessoaRequest.getNome());
		}

		if (!Objects.isNull(pessoaRequest.getDataAdmissao())) {
			pessoa.setDataAdmissao(pessoaRequest.getDataAdmissao());
		}

		if (!Objects.isNull(pessoaRequest.getDataNascimento())) {
			pessoa.setDataNascimento(pessoaRequest.getDataNascimento());
		}

		return pessoa;

	}

	public boolean isPersonExist(Long id) {
		Stream<Entry<Integer, Pessoa>> filter = this.peoplesMap.entrySet().stream()
				.filter(p -> p.getValue().getId() == id);

		return filter.count() != 0;

	}

}

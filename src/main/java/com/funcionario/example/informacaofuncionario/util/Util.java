package com.funcionario.example.informacaofuncionario.util;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.time.format.DateTimeFormatter;

import org.joda.time.Days;
import org.joda.time.LocalDate;
import org.joda.time.Months;
import org.joda.time.Years;

public class Util {

	private static final BigDecimal MIN_SALARY = new BigDecimal("1302");
	private static final BigDecimal START_SALARY = new BigDecimal("1558");

	public static int ageInYears(LocalDate birthday) {
		Years yearsBetween = Years.yearsBetween(birthday, new LocalDate());
		return yearsBetween.getYears();
	}

	public static int ageInMonths(LocalDate birthday) {
		Months monthsBetween = Months.monthsBetween(birthday, new LocalDate());
		return monthsBetween.getMonths();
	}

	public static int ageInDays(LocalDate birthday) {
		Days daysBetween = Days.daysBetween(birthday, new LocalDate());
		return daysBetween.getDays();
	}

	public static BigDecimal qtdSalaryMin(BigDecimal salary) {
		return salary.divide(MIN_SALARY, 2, RoundingMode.UP);
	}

	public static BigDecimal currentSalary(LocalDate dataAdmissao) {
		int completedYearsWork = Years.yearsBetween(dataAdmissao, new LocalDate()).getYears();

		BigDecimal currentSalary = START_SALARY;
		for (int i = 0; i < completedYearsWork; i++) {
			// calcula porcentagem 18%
			currentSalary = currentSalary.multiply(new BigDecimal("1.18"));
			// soma 500,00
			currentSalary = currentSalary.add(new BigDecimal("500"));
		}

		return currentSalary.setScale(2, RoundingMode.UP);
	}
	
	public static LocalDate convertDate(java.time.LocalDate date) {
		return new LocalDate(date.getYear(),
				date.getMonthValue(), date.getDayOfMonth());
	}

}

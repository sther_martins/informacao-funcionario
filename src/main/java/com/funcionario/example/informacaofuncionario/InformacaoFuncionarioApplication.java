package com.funcionario.example.informacaofuncionario;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class InformacaoFuncionarioApplication {

	public static void main(String[] args) {
		SpringApplication.run(InformacaoFuncionarioApplication.class, args);
	}

}

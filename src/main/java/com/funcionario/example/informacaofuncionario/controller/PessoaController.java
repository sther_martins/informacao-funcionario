package com.funcionario.example.informacaofuncionario.controller;

import java.math.BigDecimal;
import java.util.Arrays;
import java.util.Collection;
import java.util.Objects;
import java.util.TreeMap;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PatchMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.funcionario.example.informacaofuncionario.model.Pessoa;
import com.funcionario.example.informacaofuncionario.model.dto.AgeDTO;
import com.funcionario.example.informacaofuncionario.model.dto.NomeDTO;
import com.funcionario.example.informacaofuncionario.model.dto.Response;
import com.funcionario.example.informacaofuncionario.model.dto.SalaryDTO;
import com.funcionario.example.informacaofuncionario.service.PessoaService;
import com.funcionario.example.informacaofuncionario.util.Util;

@RestController
@RequestMapping("/person")
public class PessoaController {

	private PessoaService pessoaService;

	//injeção do service
	public PessoaController(PessoaService pessoaService) {
		this.pessoaService = pessoaService;

	}
	
	@GetMapping
	public ResponseEntity<Collection<Pessoa>> personList() {
		return new ResponseEntity<>(pessoaService.peoplesMap.values(), HttpStatus.OK);
	}

	@GetMapping("/{id}")
	public ResponseEntity<Response<Pessoa>> person(@PathVariable("id") Long id) {
		Response<Pessoa> response = new Response<>();
		if (!pessoaService.isPersonExist(id)) {
			response.addErrorMsgToResponse("Não existe pessoa cadastrada com o ID requisitado");
			return ResponseEntity.status(HttpStatus.NOT_FOUND).body(response);
		}

		response.setData(pessoaService.peoplesMap.get(id.intValue()));
		return new ResponseEntity<>(response, HttpStatus.OK);
	}

	@GetMapping("/{id}/salary")
	public ResponseEntity<Response<SalaryDTO>> salaryPerson(@PathVariable("id") Long id,
			@RequestParam(name = "output") String output) {
		Response<SalaryDTO> response = new Response<>();

		if (!pessoaService.isPersonExist(id)) {
			response.addErrorMsgToResponse("Não existe pessoa cadastrada com o ID requisitado");
			return ResponseEntity.status(HttpStatus.NOT_FOUND).body(response);
		}

		String[] outputs = { "full", "min" };
		if (Arrays.stream(outputs).filter(o -> o.equals(output)).count() == 0) {
			response.addErrorMsgToResponse("Parâmetro de output inválido");
			return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(response);
		}

		Pessoa pessoa = pessoaService.peoplesMap.get(id.intValue());
		BigDecimal currentSalary = Util.currentSalary(Util.convertDate(pessoa.getDataAdmissao()));
		BigDecimal qtdSalaryMin = Util.qtdSalaryMin(currentSalary);

		if (output.equalsIgnoreCase("full")) {
			response.setData(new SalaryDTO(currentSalary));
		}

		if (output.equalsIgnoreCase("min"))
			response.setData(new SalaryDTO(qtdSalaryMin));

		return new ResponseEntity<>(response, HttpStatus.OK);
	}

	@GetMapping("/{id}/age")
	public ResponseEntity<Response<AgeDTO>> agePerson(@PathVariable("id") Long id,
			@RequestParam(name = "output") String output) {
		Response<AgeDTO> response = new Response<>();

		String[] outputs = { "years", "months", "days" };
		if (Arrays.stream(outputs).filter(o -> o.equals(output)).count() == 0) {
			response.addErrorMsgToResponse("Parâmetro de output inválido");
			return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(response);
		}

		if (!pessoaService.isPersonExist(id)) {
			response.addErrorMsgToResponse("Não existe pessoa cadastrada com o ID requisitado");
			return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(response);
		}

		Pessoa pessoa = pessoaService.peoplesMap.get(id.intValue());
		int age = 0;

		if (output.equalsIgnoreCase("years"))
			age = Util.ageInYears(Util.convertDate(pessoa.getDataNascimento()));

		if (output.equalsIgnoreCase("months"))
			age = Util.ageInMonths(Util.convertDate(pessoa.getDataNascimento()));

		if (output.equalsIgnoreCase("days"))
			age = Util.ageInDays(Util.convertDate(pessoa.getDataNascimento()));

		response.setData(new AgeDTO(age));
		return new ResponseEntity<>(response, HttpStatus.OK);
	}

	@PostMapping
	public ResponseEntity<Response<Pessoa>> person(@RequestBody Pessoa pessoa) {

		Response<Pessoa> response = new Response<>();
		if (Objects.isNull(pessoa.getId())) {
			response.addErrorMsgToResponse("Id é obrigatório");
			response.setData(pessoa);
			return ResponseEntity.badRequest().body(response);
		}

		// checa se ID já existe
		if (pessoaService.isPersonExist(pessoa.getId())) {
			response.addErrorMsgToResponse("Já existe uma pessoa cadastrada com esse ID");
			response.setData(pessoa);
			return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(response);
		}

		// add pessoa
		TreeMap<Integer, Pessoa> peoples = pessoaService.peoplesMap;
		Integer key = peoples.lastKey() + 1;
		pessoa.setId(Long.valueOf(key));
		pessoaService.addUpdatePerson(key, pessoa);
		response.setData(pessoaService.peoplesMap.get(key));

		return new ResponseEntity<>(response, HttpStatus.CREATED);

	}

	@PutMapping("/{id}")
	public ResponseEntity<Response<Pessoa>> updatePerson(@PathVariable("id") Long id,
			@RequestBody Pessoa pessoaRequest) {
		Response<Pessoa> response = new Response<>();
		if (!pessoaService.isPersonExist(id)) {
			response.addErrorMsgToResponse("Não existe pessoa cadastrada com o ID requisitado");
			return ResponseEntity.status(HttpStatus.NOT_FOUND).body(response);
		}

		Integer key = id.intValue();
		Pessoa newPessoa = pessoaService.updatePerson(id, pessoaRequest);
		pessoaService.addUpdatePerson(key, newPessoa);

		response.setData(pessoaService.peoplesMap.get(key));
		return new ResponseEntity<>(response, HttpStatus.OK);
	}

	@PatchMapping("/{id}")
	public ResponseEntity<Response<Pessoa>> updatePerson(@PathVariable("id") Long id, @RequestBody NomeDTO nomeDTO) {
		Response<Pessoa> response = new Response<>();
		if (!pessoaService.isPersonExist(id)) {
			response.addErrorMsgToResponse("Não existe pessoa cadastrada com o ID requisitado");
			return ResponseEntity.status(HttpStatus.NOT_FOUND).body(response);
		}
		
		if (Objects.isNull(nomeDTO.getNome())) {
			response.addErrorMsgToResponse("Parâmetro para atualizar o registro é obrigatório");
			return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(response);
		}

		Integer key = id.intValue();
		Pessoa pessoaUpdated = pessoaService.updatePerson(id, new Pessoa(id, nomeDTO.getNome(), null, null));
		pessoaService.addUpdatePerson(key, pessoaUpdated);

		response.setData(pessoaService.peoplesMap.get(key));
		return new ResponseEntity<>(response, HttpStatus.OK);
	}

	@DeleteMapping("/{id}")
	public ResponseEntity<Response<String>> deletePerson(@PathVariable("id") Long id) {

		Response<String> response = new Response<>();
		// checa se ID já existe
		if (!pessoaService.isPersonExist(id)) {
			response.addErrorMsgToResponse("Não existe pessoa cadastrada com o ID requisitado");
			return ResponseEntity.status(HttpStatus.NOT_FOUND).body(response);
		}
		pessoaService.removePerson(id.intValue());
		response.setData("Pessoa id=" + id + " deletado com sucesso");

		return new ResponseEntity<>(response, HttpStatus.OK);

	}

}

package com.funcionario.example.informacaofuncionario.model.dto;

import java.io.Serializable;

public class AgeDTO implements Serializable {

	private static final long serialVersionUID = -6460055463633495269L;

	private int age;

	public AgeDTO(int age) {
		this.age = age;
	}

	public int getAge() {
		return age;
	}

	public void setAge(int age) {
		this.age = age;
	}

}

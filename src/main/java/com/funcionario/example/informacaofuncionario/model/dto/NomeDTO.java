package com.funcionario.example.informacaofuncionario.model.dto;

public class NomeDTO {
	
	private String nome;

	public String getNome() {
		return nome;
	}

	public void setNome(String nome) {
		this.nome = nome;
	}

	public NomeDTO(String nome) {
		this.nome = nome;
	}

	public NomeDTO() {
	}
	
	

}

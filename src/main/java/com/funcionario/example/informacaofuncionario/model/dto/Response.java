package com.funcionario.example.informacaofuncionario.model.dto;

import java.time.LocalDateTime;

public class Response<T> {

	private T data;
	private Object errors;
	
    public void addErrorMsgToResponse(String msgError) {
    	ResponseError error = new ResponseError(LocalDateTime.now(), msgError);
    	
    	setErrors(error);
    }

	public Object getErrors() {
		return errors;
	}

	public void setErrors(Object errors) {
		this.errors = errors;
	}

	public T getData() {
		return data;
	}

	public void setData(T data) {
		this.data = data;
	}
    
    
}

package com.funcionario.example.informacaofuncionario.model.dto;

import java.time.LocalDateTime;

public class ResponseError {

	private LocalDateTime timestamp;

	private String details;

	public ResponseError(LocalDateTime timestamp, String details) {
		this.timestamp = timestamp;
		this.details = details;
	}

	public LocalDateTime getTimestamp() {
		return timestamp;
	}

	public void setTimestamp(LocalDateTime timestamp) {
		this.timestamp = timestamp;
	}

	public String getDetails() {
		return details;
	}

	public void setDetails(String details) {
		this.details = details;
	}

}

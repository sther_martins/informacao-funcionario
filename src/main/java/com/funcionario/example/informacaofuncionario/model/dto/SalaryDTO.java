package com.funcionario.example.informacaofuncionario.model.dto;

import java.io.Serializable;
import java.math.BigDecimal;

public class SalaryDTO implements Serializable{

	private static final long serialVersionUID = 1L;
	private BigDecimal salary;

	public BigDecimal getSalary() {
		return salary;
	}

	public void setSalary(BigDecimal salary) {
		this.salary = salary;
	}

	public SalaryDTO(BigDecimal salary) {
		this.salary = salary;
	}

}
